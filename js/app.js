let dice1 = 0;
let dice2 = 0;
let dice = 0;
let score_Round_0 = 0;
let score_Global_1 = 0;
let score_Global_2 = 0;
let score_Global = 0;
let n = 0;

function rollDice() {
    dice1 = Math.floor(Math.random() * 6) + 1;
    dice2 = Math.floor(Math.random() * 6) + 1;
    dice = dice1 + dice2;

    if (dice1 == 1) {
        document.getElementById('mon_de').src = ("./img/1.png");
    } else if (dice1 == 2) {
        document.getElementById('mon_de').src = ("./img/2.png");
    } else if (dice1 == 3) {
        document.getElementById('mon_de').src = ("./img/3.png");
    } else if (dice1 == 4) {
        document.getElementById('mon_de').src = ("./img/4.png");
    } else if (dice1 == 5) {
        document.getElementById('mon_de').src = ("./img/5.png");
    } else if (dice1 == 6) {
        document.getElementById('mon_de').src = ("./img/6.png");
    }

    if (dice2 == 1) {
        document.getElementById('mon_de2').src = ("./img/1.png");
    } else if (dice2 == 2) {
        document.getElementById('mon_de2').src = ("./img/2.png");
    } else if (dice2 == 3) {
        document.getElementById('mon_de2').src = ("./img/3.png");
    } else if (dice2 == 4) {
        document.getElementById('mon_de2').src = ("./img/4.png");
    } else if (dice2 == 5) {
        document.getElementById('mon_de2').src = ("./img/5.png");
    } else if (dice2 == 6) {
        document.getElementById('mon_de2').src = ("./img/6.png");
    }

    if (dice == 2 && (n == 0 || n == 1)) {
        score_Round_0 = 0;
        dice = 0;
        document.getElementById("score_round_joueur_" + (n + 1)).innerHTML = score_Round_0;
        if (n == 0) {
            n = 1;
        } else {
            n = 0;
        }
    } else if (dice > 2) {
        score_Round_0 += dice;
        document.getElementById("score_round_joueur_" + (n + 1)).innerHTML = score_Round_0;
    }
}

function hold() {
    //if (score_Round_0 > 0) {
    if (n == 0) {
        score_Global_1 += score_Round_0;
        score_Global = score_Global_1;
    } else {
        score_Global_2 += score_Round_0;
        score_Global = score_Global_2;
    }

    document.getElementById("global_joueur_" + (n + 1)).innerHTML = score_Global;
    score_Round_0 = 0;
    document.getElementById("score_round_joueur_" + (n + 1)).innerHTML = score_Round_0;
    if (score_Global >= 100) {
        document.getElementById("global_joueur_" + (n + 1)).innerHTML = score_Round_0;
        alert("le joueur " + (n + 1) + " a gagné!!!")
    }
    if (n == 0) {
        n = 1;
    } else {
        n = 0;
    }
    //}
}

function newGame() {
    score_Round_0 = 0;
    score_Global_1 = 0;
    score_Global_2 = 0;
    document.getElementById("score_round_joueur_1").innerHTML = "Score Round";
    document.getElementById("score_round_joueur_2").innerHTML = "Score Round";
    document.getElementById("global_joueur_1").innerHTML = "Score Global";
    document.getElementById("global_joueur_2").innerHTML = "Score Global";
    document.getElementById('mon_de').src = ("./img/1.png");
    document.getElementById('mon_de2').src = ("./img/1.png");
}